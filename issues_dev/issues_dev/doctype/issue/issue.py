# -*- coding: utf-8 -*-
# Copyright (c) 2017, Pau Rosello and contributors
# For license information, please see license.txt

from __future__ import unicode_literals
import frappe
from frappe.email.smtp import get_outgoing_email_account
from frappe.model.document import Document

class Issue(Document):
	def on_update(self):
		config = frappe.get_doc("Issues Config")
		email_account = get_outgoing_email_account(True)
		if email_account.enable_outgoing == 1:
			frappe.sendmail(recipients=config.email_destination,
							subject= self.title + ": " + self.status,
							message=config.url+"/desk#Form/Issue/" + self.name + "\n\n\n\n <br><br>" + self.description,
							sender="paur@servipro.eu")